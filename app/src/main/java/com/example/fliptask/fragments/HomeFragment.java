package com.example.fliptask.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.fliptask.ApiClient;
import com.example.fliptask.BaseService;
import com.example.fliptask.R;
import com.example.fliptask.adapter.CoinListAdapter;
import com.example.fliptask.model.CoinsList;
import com.example.fliptask.model.SharedPrefrenceData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {
    RecyclerView rvHome;
    Context mContext;
    LinearLayoutManager layoutManager;
    private CoinListAdapter coinListAdapter;
    ProgressBar progressBar;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        progressBar = view.findViewById(R.id.progress_circular);
        rvHome = view.findViewById(R.id.rv_home);

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvHome.setLayoutManager(layoutManager);


        callApi();
        return view;
    }

    void callApi() {
        ApiClient mServiceApiClient = BaseService.getRetroFitClient();
        Call<List<CoinsList>> call = mServiceApiClient.getResponse();
        progressBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<List<CoinsList>>() {
            @Override
            public void onResponse(Call<List<CoinsList>> call, Response<List<CoinsList>> response) {

                coinListAdapter = new CoinListAdapter(response.body(), mContext);
                rvHome.setAdapter(coinListAdapter);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<CoinsList>> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });


    }
}
