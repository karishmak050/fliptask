package com.example.fliptask.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

/**
 * Created by CRIMSON-2 on 7/9/2018.
 */


public class BaseFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    public void replace(int containerId, Fragment fragment, boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(containerId, fragment, Integer.toString(getFragmentManager().getBackStackEntryCount()));
        if (addToBackStack) ft.addToBackStack(null);
        ft.commit();
        fm.executePendingTransactions();
        Log.d("check","replace");
    }
}
