package com.example.fliptask.fragments;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.fliptask.ApiClient;
import com.example.fliptask.BaseService;
import com.example.fliptask.R;
import com.example.fliptask.adapter.CoinListAdapter;
import com.example.fliptask.adapter.EventAdapter;
import com.example.fliptask.adapter.WatcherAdapter;
import com.example.fliptask.model.CoinsList;
import com.example.fliptask.model.EventResponse;
import com.example.fliptask.model.SharedPrefrenceData;
import com.example.fliptask.model.WatcherData;
import com.example.fliptask.model.WatcherResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class WatcherFragment extends BaseFragment {

    RecyclerView rvWatcher;
    LinearLayoutManager layoutManager;
    private WatcherAdapter watcherAdapter;
    String id;
    CoinsList coinsList = null;
    List<CoinsList> coinsListsarray = new ArrayList<>();
    ProgressBar progressBar;

    // private SharedPrefrenceData sharedPrefrenceData;

    public WatcherFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            coinsList = (CoinsList) bundle.getSerializable("obj");
            coinsListsarray.add(coinsList);
            SharedPrefrenceData.newInstance().saveList(getActivity(), coinsListsarray, SharedPrefrenceData.COIN_LIST_DATA);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_watcher, container, false);
        progressBar = view.findViewById(R.id.progress_circular);

        rvWatcher = view.findViewById(R.id.rv_watcher);

        coinsListsarray.clear();

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvWatcher.setLayoutManager(layoutManager);


        callApi();

        return view;
    }


    void callApi() {
        ApiClient mServiceApiClient = BaseService.getRetroFitClient();
        progressBar.setVisibility(View.VISIBLE);

        Call<WatcherData> call = mServiceApiClient.getWatcherResponse();
        call.enqueue(new Callback<WatcherData>() {
            @Override
            public void onResponse(Call<WatcherData> call, Response<WatcherData> response) {
                progressBar.setVisibility(View.GONE);
                if (SharedPrefrenceData.newInstance().getList(getActivity(), SharedPrefrenceData.COIN_LIST_DATA) != null &&
                        SharedPrefrenceData.newInstance().getList(getActivity(), SharedPrefrenceData.COIN_LIST_DATA).size() > 0) {
                    coinsListsarray.addAll(SharedPrefrenceData.newInstance().getList(getActivity(), SharedPrefrenceData.COIN_LIST_DATA));
                    watcherAdapter = new WatcherAdapter(getActivity(), response.body(), coinsListsarray);
                    rvWatcher.setAdapter(watcherAdapter);
                } else {
                    Toast.makeText(getActivity(), "No data added in watcher list", Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<WatcherData> call, Throwable throwable) {
                Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });


    }


}
