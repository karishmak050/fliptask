package com.example.fliptask.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.fliptask.ApiClient;
import com.example.fliptask.BaseService;
import com.example.fliptask.R;
import com.example.fliptask.adapter.CoinListAdapter;
import com.example.fliptask.adapter.EventAdapter;
import com.example.fliptask.model.EventResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends BaseFragment {

    RecyclerView rvEvent;
    LinearLayoutManager layoutManager;
    private EventAdapter eventAdapter;
    ProgressBar progressBar;

    private Context mContext;

    public EventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event, container, false);
        progressBar = view.findViewById(R.id.progress_circular);

        rvEvent = view.findViewById(R.id.rv_event);
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvEvent.setLayoutManager(layoutManager);

        callApi();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = getActivity();
    }


    void callApi(){
        ApiClient mServiceApiClient = BaseService.getRetroFitClient();
        progressBar.setVisibility(View.VISIBLE);

        Call<EventResponse> call = mServiceApiClient.getEventResponse();
        call.enqueue(new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                if (response.body() != null){

                    eventAdapter = new EventAdapter(mContext,response.body().getDataList());
                    rvEvent.setAdapter(eventAdapter);

                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {
                Log.e("TestError",t.getMessage());
                Toast.makeText(mContext,"Something went wrong",Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });
    }

}
