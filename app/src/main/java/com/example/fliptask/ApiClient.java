package com.example.fliptask;


import com.example.fliptask.model.CoinsList;
import com.example.fliptask.model.EventResponse;
import com.example.fliptask.model.WatcherData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Karishma on 13/12/18.
 */
public interface ApiClient {

  @GET(AppConstant.GET_URL)
  Call<List<CoinsList>> getResponse();

  @GET(AppConstant.EVENTS_API)
  Call<EventResponse> getEventResponse();

  @GET(AppConstant.WATCHER_API)
  Call<WatcherData> getWatcherResponse();
}
