package com.example.fliptask;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.example.fliptask.fragments.EventFragment;
import com.example.fliptask.fragments.HomeFragment;
import com.example.fliptask.fragments.WatcherFragment;
import com.example.fliptask.model.SharedPrefrenceData;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity extends BaseActivity {
    private ActionBar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = getSupportActionBar();
        replace(R.id.container, new HomeFragment(), false);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        toolbar.setTitle("List Of Coins");


    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_shop:
                    toolbar.setTitle("List Of Coins");
                    replace(R.id.container, new HomeFragment(), false);
                    return true;
                case R.id.navigation_gifts:
                    toolbar.setTitle("Coin watcher");

                    replace(R.id.container, new WatcherFragment(), false);

                    return true;
                case R.id.navigation_cart:
                    toolbar.setTitle("Events");
                    replace(R.id.container, new EventFragment(), false);
                    return true;

            }
            return false;
        }
    };

}
