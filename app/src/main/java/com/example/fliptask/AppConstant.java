package com.example.fliptask;

/**
 * Created by Karishma on 13/12/18.
 */
public class AppConstant {

  public static final String GET_URL = "coins/list";
  public static final String EVENTS_API = "events";
  public static final String WATCHER_API = "coins/01coin";
}
