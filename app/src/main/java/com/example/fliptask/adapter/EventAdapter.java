package com.example.fliptask.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fliptask.R;
import com.example.fliptask.model.EventData;

import java.util.ArrayList;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<EventData> eventDataArrayList;

    public EventAdapter(Context mContext, ArrayList<EventData> eventDataArrayList) {
        this.mContext = mContext;
        this.eventDataArrayList = eventDataArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_event,viewGroup,false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        EventData data = eventDataArrayList.get(i);
        viewHolder.tvTitle.setText(data.getTitle());
        viewHolder.tvStartDate.setText("Start date: "+data.getStart_date());
        viewHolder.tvEndDate.setText("End date: "+data.getEnd_date());
        viewHolder.tvDesc.setText(data.getDescription());
        viewHolder.tvCountry.setText("Country: "+data.getCountry());
        viewHolder.tvCity.setText("City: "+data.getCity());
      Glide.with(mContext).load(data.getScreenshot()).into(viewHolder.imageView);



    }

    @Override
    public int getItemCount() {
        return eventDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle,tvStartDate,tvEndDate,tvDesc,tvCountry,tvCity;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle= itemView.findViewById(R.id.tv_title);
            tvStartDate= itemView.findViewById(R.id.tv_startdate);
            tvEndDate= itemView.findViewById(R.id.tv_enddate);
            tvDesc= itemView.findViewById(R.id.tv_desc);
            tvCountry= itemView.findViewById(R.id.tv_country);
            tvCity= itemView.findViewById(R.id.tv_city);
            imageView = itemView.findViewById(R.id.img);
        }
    }
}
