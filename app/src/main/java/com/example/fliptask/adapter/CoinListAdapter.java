package com.example.fliptask.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fliptask.MainActivity;
import com.example.fliptask.R;
import com.example.fliptask.fragments.HomeFragment;
import com.example.fliptask.fragments.WatcherFragment;
import com.example.fliptask.model.CoinsList;
import com.example.fliptask.model.SharedPrefrenceData;

import java.util.List;

public class CoinListAdapter extends RecyclerView.Adapter<CoinListAdapter.ViewHolder> {

    List<CoinsList> coinarrList;
    private Context mContext;


    public CoinListAdapter(List<CoinsList> coinarrList, Context mContext) {
        this.coinarrList = coinarrList;
        this.mContext = mContext;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        final CoinsList coinsList = coinarrList.get(i);
        viewHolder.tvId.setText("Id: " + coinsList.getId());
        viewHolder.tvName.setText("Name: " + coinsList.getName());
        viewHolder.tvSymbol.setText("Symbol: " + coinsList.getSymbol());


    }


    @Override
    public int getItemCount() {
        return coinarrList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvId, tvName, tvSymbol, tvAdd;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tv_id);
            tvName = itemView.findViewById(R.id.tv_name);
            tvSymbol = itemView.findViewById(R.id.tv_symbol);
            tvAdd = itemView.findViewById(R.id.tv_add);

            tvAdd.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tv_add:

                    int pos = getLayoutPosition();
                    CoinsList mCoinsList = coinarrList.get(pos);
                    MainActivity activity = (MainActivity) view.getContext();


                    WatcherFragment myFragment = new WatcherFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("obj", mCoinsList);
                    myFragment.setArguments(bundle);

                    Log.d("dataMe", "CoinListAdptr: -->" + mCoinsList.getName());

                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.container, myFragment).addToBackStack(null).commit();
                    break;
            }
        }
    }
}

