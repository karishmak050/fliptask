package com.example.fliptask.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fliptask.R;
import com.example.fliptask.model.CoinsList;
import com.example.fliptask.model.EventData;
import com.example.fliptask.model.WatcherData;

import java.util.ArrayList;
import java.util.List;

public class WatcherAdapter extends RecyclerView.Adapter<WatcherAdapter.ViewHolder> {

    List<String> watcherDataArrayList;
    private Context mContext;
    WatcherData watcherData = null;
    CoinsList coinsList;
    List<CoinsList> coinsListsarray;


    public WatcherAdapter(Context mContext, WatcherData watcherData, List<CoinsList> coinsListsarray) {
        this.mContext = mContext;
        this.watcherData = watcherData;
        // this.watcherDataArrayList = watcherData.getCategoriesList();
        this.coinsListsarray = coinsListsarray;
        Log.d("dataMe", "WatcherAdptr--> " + coinsListsarray.size());
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_watcher, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        /*  viewHolder.tvId.setText(watcherData.getId());
        viewHolder.tvSymbol.setText("start date: "+watcherData.getSymbol());
        viewHolder.tvName.setText("end date: "+watcherData.getName());*/
        // viewHolder.tvName.setText("end date: "+watcherData.getCategoriesList());
        Log.d("datttaa", "size" + coinsListsarray.size());

        coinsList = coinsListsarray.get(i);
        Log.d("dataa", coinsList.getId());
        viewHolder.tvId.setText("Id: " + coinsList.getId());
        viewHolder.tvSymbol.setText("Symbol: " + coinsList.getSymbol());
        viewHolder.tvName.setText("Name: " + coinsList.getName());
    }

    @Override
    public int getItemCount() {
        return coinsListsarray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvId, tvSymbol, tvName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tv_wat_id);
            tvSymbol = itemView.findViewById(R.id.tv_wat_symbol);
            tvName = itemView.findViewById(R.id.tv_wat_name);
        }
    }
}
