package com.example.fliptask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EventResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<EventData> dataList = new ArrayList<>();

    public ArrayList<EventData> getDataList() {
        return dataList;
    }

    public void setDataList(ArrayList<EventData> dataList) {
        this.dataList = dataList;
    }
}
