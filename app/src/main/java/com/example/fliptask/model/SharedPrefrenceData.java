package com.example.fliptask.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPrefrenceData {

    public static String COIN_LIST_DATA = "coin_list_data";
    public static String COMING_FROM_COIN_LIST = "coming_from_coin_list";


    private static SharedPrefrenceData sharedPrefrencesData = null;

    private SharedPrefrenceData() {

    }

    public static synchronized SharedPrefrenceData newInstance() {
        if (sharedPrefrencesData == null) {
            sharedPrefrencesData = new SharedPrefrenceData();
        }
        return sharedPrefrencesData;
    }

    public void saveList(Context context, List<CoinsList> arrayList, String key) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        editor = settings.edit();
        Gson gson = new Gson();
        String value = gson.toJson(arrayList);
        editor.putString(key, value);
        editor.commit();
    }

    public ArrayList<CoinsList> getList(Context context, String key) {

        SharedPreferences settings;
        settings = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String value = settings.getString(key, null);
        Type type = new TypeToken<ArrayList<CoinsList>>() {
        }.getType();
        ArrayList<CoinsList> arrayList = gson.fromJson(value, type);
        return arrayList;
    }


    public void saveBoolean(Context context, boolean value, String key) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(Context context, String key) {
        SharedPreferences settings;
        settings = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return settings.getBoolean(key, false);

    }

}
